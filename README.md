# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Quick Summary ###
This application allows the user to start or end a shift. It also shows the list of previously completed shifts.


### Tech Stack ###

* Kotlin
* MVVM and CLEAN Architecture
* Coroutines
* Koin
* Glide
