package com.deputy.shifts

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.deputy.shifts.data.network.models.response.ShiftResponse
import com.deputy.shifts.data.network.services.ShiftService
import com.deputy.shifts.domain.models.Location
import com.deputy.shifts.domain.repositories.ShiftRepository
import com.deputy.shifts.domain.repositories.ShiftRepositoryImpl
import com.deputy.shifts.domain.usecases.EndShiftUsecase
import com.deputy.shifts.domain.usecases.FetchPreviousShiftsUsecase
import com.deputy.shifts.domain.usecases.StartShiftUsecase
import com.deputy.shifts.presentation.Error
import com.deputy.shifts.presentation.ShiftSharedViewModel
import com.deputy.shifts.presentation.Success
import com.deputy.shifts.presentation.ViewState
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.setMain
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.ArgumentCaptor
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ShiftSharedViewModelTest : KoinTest {

    private val viewModel: ShiftSharedViewModel by inject()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var previousShiftsListViewState: Observer<ViewState>

    @Mock
    lateinit var startShiftViewState: Observer<ViewState>

    @Mock
    lateinit var endShiftViewState: Observer<ViewState>

    @Mock
    lateinit var service: ShiftService

    @ExperimentalCoroutinesApi
    private val testDispatcher = TestCoroutineDispatcher()
    
    private val appModule = module {
        factory<ShiftRepository> { (ShiftRepositoryImpl(service)) }
        factory { FetchPreviousShiftsUsecase(get()) }
        factory { StartShiftUsecase(get()) }
        factory { EndShiftUsecase(get()) }
        single { ShiftSharedViewModel(get(), get(), get()) }
    }

    @ExperimentalCoroutinesApi
    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        startKoin {
            modules(appModule)
        }
        Dispatchers.setMain(testDispatcher)
    }

    @ExperimentalCoroutinesApi
    @After
    fun after() {
        stopKoin()
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun viewModel_returns_success_view_state_when_getPreviousShifts_api_returns_success() {
        runBlocking {
            //given
            val shiftResponse = ShiftResponse(1,
                    "2020-10-15T04:04:18Z",
                    "2020-10-15T04:04:18Z",
                    "-33.7906848",
                    "150.9472644",
                    "-33.7906848",
                    "150.9472644",
                    "https://unsplash.it/500/500?random")
            whenever(service.getPreviousShifts()).thenReturn(listOf(shiftResponse))
            viewModel.getPreviousShiftsListViewState().observeForever(previousShiftsListViewState)
            val captor = ArgumentCaptor.forClass(ViewState::class.java)

            //when
            viewModel.getPreviousShifts()

            //then
            captor.run {
                (this as ArgumentCaptor<ViewState>)!!.let {
                    Mockito.verify(previousShiftsListViewState, Mockito.atLeast(1)).onChanged(capture())
                    assertThat(value, instanceOf(Success::class.java))
                }
            }
        }
    }

    @Test
    fun viewModel_returns_error_view_state_when_getPreviousShifts_api_returns_error() {
        runBlocking {
            //given
            whenever(service.getPreviousShifts()).thenThrow(RuntimeException("Api error"))
            viewModel.getPreviousShiftsListViewState().observeForever(previousShiftsListViewState)
            val captor = ArgumentCaptor.forClass(ViewState::class.java)

            //when
            viewModel.getPreviousShifts()

            //then
            captor.run {
                (this as ArgumentCaptor<ViewState>)!!.let {
                    Mockito.verify(previousShiftsListViewState, Mockito.atLeast(1)).onChanged(capture())
                    assertThat(value, instanceOf(Error::class.java))
                }
            }
        }
    }

    @Test
    fun viewModel_returns_success_view_state_when_startShifts_api_returns_success() {
        runBlocking {
            //given
            whenever(service.startShift(any())).thenReturn(mock())
            viewModel.getStartShiftViewState().observeForever(startShiftViewState)
            val captor = ArgumentCaptor.forClass(ViewState::class.java)

            //when
            viewModel.startShift(Location(-33.7906848, 150.9472644))

            //then
            captor.run {
                (this as ArgumentCaptor<ViewState>)!!.let {
                    Mockito.verify(startShiftViewState, Mockito.atLeast(1)).onChanged(capture())
                    assertThat(value, instanceOf(Success::class.java))
                }
            }
        }
    }

    @Test
    fun viewModel_returns_error_view_state_when_startShifts_api_returns_error() {
        runBlocking {
            //given
            whenever(service.startShift(any())).thenThrow(RuntimeException("Api error"))
            viewModel.getStartShiftViewState().observeForever(startShiftViewState)
            val captor = ArgumentCaptor.forClass(ViewState::class.java)

            //when
            viewModel.startShift(Location(-33.7906848, 150.9472644))

            //then
            captor.run {
                (this as ArgumentCaptor<ViewState>)!!.let {
                    Mockito.verify(startShiftViewState, Mockito.atLeast(1)).onChanged(capture())
                    assertThat(value, instanceOf(Error::class.java))
                }
            }
        }
    }

    @Test
    fun viewModel_returns_success_view_state_when_endShifts_api_returns_success() {
        runBlocking {
            //given
            whenever(service.endShift(any())).thenReturn(mock())
            viewModel.getEndShiftViewState().observeForever(endShiftViewState)
            val captor = ArgumentCaptor.forClass(ViewState::class.java)

            //when
            viewModel.endShift(Location(-33.7906848, 150.9472644))

            //then
            captor.run {
                (this as ArgumentCaptor<ViewState>)!!.let {
                    Mockito.verify(endShiftViewState, Mockito.atLeast(1)).onChanged(capture())
                    assertThat(value, instanceOf(Success::class.java))
                }
            }
        }
    }

    @Test
    fun viewModel_returns_error_view_state_when_endShifts_api_returns_error() {
        runBlocking {
            //given
            whenever(service.endShift(any())).thenThrow(RuntimeException("Api error"))
            viewModel.getEndShiftViewState().observeForever(endShiftViewState)
            val captor = ArgumentCaptor.forClass(ViewState::class.java)

            //when
            viewModel.endShift(Location(-33.7906848, 150.9472644))

            //then
            captor.run {
                (this as ArgumentCaptor<ViewState>)!!.let {
                    Mockito.verify(endShiftViewState, Mockito.atLeast(1)).onChanged(capture())
                    assertThat(value, instanceOf(Error::class.java))
                }
            }
        }
    }
}
