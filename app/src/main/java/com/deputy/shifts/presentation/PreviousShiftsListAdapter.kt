package com.deputy.shifts.presentation

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.deputy.shifts.R
import com.deputy.shifts.domain.models.Shift
import kotlinx.android.synthetic.main.layout_previous_shifts_item.view.*

class PreviousShiftsListAdapter(private val handler: ClickEventHandler) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var previousShifts: List<Shift> = ArrayList()

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_previous_shifts_item, parent, false)
        return PreviousShiftsViewHolder(view)
    }

    override fun onBindViewHolder(
            holder: RecyclerView.ViewHolder,
            position: Int
    ) {
        val previousShiftsViewHolder = holder as PreviousShiftsViewHolder
        previousShiftsViewHolder.initViews(previousShifts[position])
        previousShiftsViewHolder.itemView.setOnClickListener {
            handler.handleClick(previousShifts[position])
        }
    }

    override fun getItemCount(): Int {
        return previousShifts.size
    }

    inner class PreviousShiftsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val shiftImageView: ImageView = itemView.previous_shift_item_img
        private val shiftStartedTimeTextView: TextView = itemView.previous_shift_item_started_time
        private val shiftEndedTimeTextView: TextView = itemView.previous_shift_item_ended_time

        @SuppressLint("SetTextI18n")
        fun initViews(shift: Shift) {
            Glide.with(itemView)
                    .load(shift.imageUrl)
                    .circleCrop()
                    .into(shiftImageView)
            shiftStartedTimeTextView.text = "${getDateFromUTCString(shift.startTime)}   ${getTimeFromUTCString(shift.startTime)}"
            shiftEndedTimeTextView.text = "${getDateFromUTCString(shift.endTime)}   ${getTimeFromUTCString(shift.endTime)}"
        }
    }

    interface ClickEventHandler {
        fun handleClick(item: Shift)
    }
}