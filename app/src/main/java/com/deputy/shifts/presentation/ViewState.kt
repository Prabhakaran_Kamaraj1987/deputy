package com.deputy.shifts.presentation

import com.deputy.shifts.domain.exceptions.DomainException

/**
 * Base class to represent common UI States
 */
sealed class ViewState

object Loading : ViewState()

class Error(val exception: DomainException) : ViewState()

class Success<T>(val data: T) : ViewState()