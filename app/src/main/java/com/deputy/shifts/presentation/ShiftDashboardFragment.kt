package com.deputy.shifts.presentation

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.deputy.shifts.R
import com.deputy.shifts.domain.exceptions.mapToDomainException
import com.deputy.shifts.domain.models.Location
import com.deputy.shifts.domain.models.Shift
import com.google.android.gms.location.*
import kotlinx.android.synthetic.main.layout_dashboard_fragment.*
import org.koin.android.ext.android.get

class ShiftDashboardFragment : Fragment(), PreviousShiftsListAdapter.ClickEventHandler {

    private lateinit var viewModel: ShiftSharedViewModel
    private lateinit var previousTripsListAdapter: PreviousShiftsListAdapter
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationManager: LocationManager
    private lateinit var locationRequest: LocationRequest

    companion object {
        private const val REQUEST_FINE_LOCATION = 1
        private val TAG = ShiftDashboardFragment::class.simpleName

        fun newInstance() = ShiftDashboardFragment()
    }

    private val locationRequestCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            if (locationResult == null) {
                showLocationNotAvailableDialog()
                enableStartOrEndShiftButton(true)
            } else {
                val locations = locationResult.locations
                val lastKnownLocation = locations[locations.size - 1]
                startOrEndShift(lastKnownLocation)
                fusedLocationClient.removeLocationUpdates(this)
            }
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.layout_dashboard_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = get()
        initViews()
        initObservers()
        initLocationProviders()
    }

    private fun initViews() {
        previous_trips_list.layoutManager = LinearLayoutManager(context)
        previousTripsListAdapter = PreviousShiftsListAdapter(this)
        previous_trips_list.adapter = previousTripsListAdapter
        start_or_end_shift_btn.isSelected = false
        start_or_end_shift_btn.setOnClickListener {
            handleStartOrEndShiftButtonClicked()
        }
    }

    private fun initLocationProviders() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        locationManager = requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 0L
    }

    private fun initObservers() {
        observePreviousShifts()
        observeStartShiftViewState()
        observeEndShiftViewState()
    }

    private fun observePreviousShifts() {
        viewModel.getPreviousShiftsListViewState().observe(viewLifecycleOwner, Observer {
            when (it) {
                is Loading -> {
                    showPreviousShiftsProgressView()
                }
                is Error -> {
                    Log.e(
                            TAG,
                            "Error occurred while fetching previous shifts ${mapToDomainException(it.exception)}"
                    )
                    showPreviousShiftsNotFoundView()
                }
                is Success<*> -> {
                    if (it.data != null) {
                        val shifts = it.data as List<Shift>
                        if (shifts.isNotEmpty()) {
                            previousTripsListAdapter.previousShifts = shifts
                            previousTripsListAdapter.notifyDataSetChanged()
                            showPreviousShiftsView()
                        } else {
                            showPreviousShiftsNotFoundView()
                        }
                    }
                }
            }
        })
    }

    private fun observeStartShiftViewState() {
        viewModel.getStartShiftViewState().observe(viewLifecycleOwner, Observer {
            when (it) {
                is Loading -> {
                    handleStartOrStopShiftLoadingState(true)
                }
                is Error -> {
                    handleStartOrStopShiftLoadingState(false)
                    Log.e(
                            TAG,
                            "Error occurred while starting shift ${mapToDomainException(it.exception)}"
                    )
                    Toast.makeText(
                            requireActivity(),
                            "Error starting shift !. Please try again",
                            Toast.LENGTH_SHORT
                    ).show()
                }
                is Success<*> -> {
                    handleStartOrStopShiftLoadingState(false)
                    toggleStartOrEndShiftButton(true)
                }
            }
        })
    }

    private fun observeEndShiftViewState() {
        viewModel.getEndShiftViewState().observe(viewLifecycleOwner, Observer {
            when (it) {
                is Loading -> {
                    handleStartOrStopShiftLoadingState(true)
                }
                is Error -> {
                    handleStartOrStopShiftLoadingState(false)
                    Log.e(
                            TAG,
                            "Error occurred while ending shifts ${mapToDomainException(it.exception)}"
                    )
                    Toast.makeText(
                            requireActivity(),
                            "Error ending shift !. Please try again",
                            Toast.LENGTH_SHORT
                    ).show()
                }
                is Success<*> -> {
                    handleStartOrStopShiftLoadingState(false)
                    toggleStartOrEndShiftButton(false)
                }
            }
        })
    }

    override fun handleClick(item: Shift) {
        viewModel.setPreviousShiftItemSelected(item)
        (requireActivity() as OnPreviousShiftItemSelectedListener).onPreviousShiftItemClicked()
    }

    @SuppressWarnings("MissingPermission")
    private fun handleStartOrEndShiftButtonClicked() {
        if (hasLocationEnabled()) {
            if (hasLocationPermission()) {
                fusedLocationClient.lastLocation
                        .addOnSuccessListener(requireActivity()) { deviceLocation ->
                            if (deviceLocation != null) {
                                startOrEndShift(deviceLocation)
                            } else {
                                enableStartOrEndShiftButton(false)
                                fusedLocationClient.requestLocationUpdates(locationRequest,
                                        locationRequestCallback,
                                        Looper.getMainLooper())
                            }
                        }
            } else {
                requestLocationPermission()
            }
        } else {
            showLocationEnableDialog()
        }
    }

    private fun startOrEndShift(deviceLocation: android.location.Location) {
        val location =
                Location(deviceLocation.latitude, deviceLocation.longitude)
        if (start_or_end_shift_btn.isSelected) {
            viewModel.endShift(location)
        } else {
            viewModel.startShift(location)
        }
    }

    private fun hasLocationPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestLocationPermission() {
        requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_FINE_LOCATION
        )
    }

    private fun hasLocationEnabled(): Boolean {
        var gpsEnabled = false
        var networkEnabled = false
        try {
            gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (ex: Exception) {
        }
        try {
            networkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        } catch (ex: Exception) {
        }
        return gpsEnabled || networkEnabled
    }

    private fun showLocationEnableDialog() {
        AlertDialog.Builder(requireActivity())
                .setTitle(R.string.app_name)
                .setMessage(R.string.location_not_enabled_alert_dialog_message)
                .setPositiveButton(
                        R.string.enable_location
                ) { _, _ ->
                    requireActivity().startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }
                .setNegativeButton(R.string.cancel, null)
                .show()
    }

    private fun showLocationNotAvailableDialog() {
        AlertDialog.Builder(requireActivity())
                .setTitle(R.string.app_name)
                .setMessage(R.string.location_not_available_alert_dialog_message)
                .setNegativeButton(R.string.cancel, null)
                .show()
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_FINE_LOCATION -> {
                if (grantResults.isNotEmpty() &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    handleStartOrEndShiftButtonClicked()
                } else {
                    AlertDialog.Builder(requireActivity())
                            .setCancelable(true)
                            .setTitle(R.string.app_name)
                            .setMessage(getString(R.string.location_permission_alert_dialog_message))
                            .setNegativeButton(R.string.cancel, null)
                            .show()
                }
            }
        }
    }

    private fun showPreviousShiftsProgressView() {
        previous_shifts_progress.show()
        previous_trips_list.hide()
        no_previous_trips_found_txt.hide()
    }

    private fun showPreviousShiftsView() {
        previous_shifts_progress.hide()
        previous_trips_list.show()
        no_previous_trips_found_txt.hide()
    }

    private fun showPreviousShiftsNotFoundView() {
        previous_shifts_progress.hide()
        previous_trips_list.hide()
        no_previous_trips_found_txt.show()
    }

    private fun handleStartOrStopShiftLoadingState(isLoading: Boolean) {
        if (isLoading) {
            enableStartOrEndShiftButton(false)
            setLoadingText(!start_or_end_shift_btn.isSelected)
        } else {
            enableStartOrEndShiftButton(true)
            setLoadingText(!start_or_end_shift_btn.isSelected)
        }
    }

    private fun enableStartOrEndShiftButton(isEnabled: Boolean) {
        if (isEnabled) {
            start_or_end_shift_btn.isEnabled = true
            start_or_end_shift_btn.alpha = 1.0f
        } else {
            start_or_end_shift_btn.isEnabled = false
            start_or_end_shift_btn.alpha = 0.5f
        }
    }

    private fun setLoadingText(isStarting: Boolean) {
        if (isStarting) {
            start_or_end_shift_btn.text = getString(R.string.shift_starting)
        } else {
            start_or_end_shift_btn.text = getString(R.string.shift_ending)
        }
    }

    private fun toggleStartOrEndShiftButton(isStarted: Boolean) {
        if (isStarted) {
            start_or_end_shift_message.text = getString(R.string.dashboard_shift_started_message, getFormattedCurrentTimeString())
            start_or_end_shift_btn.text = getString(R.string.dashboard_end_shift)
            start_or_end_shift_btn.isSelected = true
        } else {
            start_or_end_shift_message.text = getString(R.string.dashboard_ready_to_start_shift)
            start_or_end_shift_btn.text = getString(R.string.dashboard_start_shift)
            start_or_end_shift_btn.isSelected = false
            viewModel.getPreviousShifts()
        }
    }

    interface OnPreviousShiftItemSelectedListener {
        fun onPreviousShiftItemClicked()
    }
}