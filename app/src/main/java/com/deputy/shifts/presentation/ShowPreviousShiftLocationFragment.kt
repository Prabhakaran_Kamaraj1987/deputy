package com.deputy.shifts.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import com.deputy.shifts.Properties.Companion.GOOGLE_MAPS_CAMERA_DEFAULT_ZOOM
import com.deputy.shifts.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.layout_show_previous_shift_location.*
import org.koin.android.ext.android.get

class ShowPreviousShiftLocationFragment : DialogFragment(), OnMapReadyCallback {

    lateinit var viewModel: ShiftSharedViewModel
    private lateinit var googleMap: GoogleMap

    companion object {
        fun newInstance() = ShowPreviousShiftLocationFragment()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.layout_show_previous_shift_location, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = get()
        initMapView(savedInstanceState)
    }

    private fun initMapView(bundle: Bundle?) {
        previous_shift_location_map_view.onCreate(bundle)
        previous_shift_location_map_view.getMapAsync(this)
        previous_shift_location_map_view.onResume()
    }

    private fun observeSelectedPreviousShiftLocation() {
        viewModel.getPreviousShiftItemSelected().observe(viewLifecycleOwner, Observer {
            // Add started and ended location markers in map and move the camera
            if (it.startLat.isNotEmpty() && it.startLong.isNotEmpty()) {
                val startedLocation = LatLng(it.startLat.toDouble(), it.startLong.toDouble())
                googleMap.addMarker(
                        MarkerOptions()
                                .position(startedLocation)
                                .title("Started location")
                )
                googleMap.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                                startedLocation,
                                GOOGLE_MAPS_CAMERA_DEFAULT_ZOOM
                        )
                )
                previous_shift_location_map_view.onResume()
            }
            if (it.endLat.isNotEmpty() && it.endLong.isNotEmpty()) {
                val endedLocation = LatLng(it.endLat.toDouble(), it.endLong.toDouble())
                googleMap.addMarker(
                        MarkerOptions()
                                .position(endedLocation)
                                .title("Ended location")
                )
            }
        })
    }

    override fun onMapReady(map: GoogleMap) {
        googleMap = map
        observeSelectedPreviousShiftLocation()
    }
}