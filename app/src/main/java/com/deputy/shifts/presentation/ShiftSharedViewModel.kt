package com.deputy.shifts.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.deputy.shifts.data.network.models.requests.StartOrEndShiftApiRequest
import com.deputy.shifts.domain.exceptions.mapToDomainException
import com.deputy.shifts.domain.models.Location
import com.deputy.shifts.domain.models.Shift
import com.deputy.shifts.domain.usecases.EndShiftUsecase
import com.deputy.shifts.domain.usecases.FetchPreviousShiftsUsecase
import com.deputy.shifts.domain.usecases.StartShiftUsecase
import kotlinx.coroutines.launch

class ShiftSharedViewModel(
        private val fetchPreviousShiftsUsecase: FetchPreviousShiftsUsecase,
        private val startShiftUsecase: StartShiftUsecase,
        private val endShiftUsecase: EndShiftUsecase
) : ViewModel() {

    private val startShiftViewState: MutableLiveData<ViewState> = MutableLiveData()
    private val endShiftViewState: MutableLiveData<ViewState> = MutableLiveData()
    private val previousShiftsListViewState: MutableLiveData<ViewState> = MutableLiveData()
    private val previousShiftItemSelected: MutableLiveData<Shift> = MutableLiveData()

    init {
        getPreviousShifts()
    }

    fun getPreviousShifts() {
        viewModelScope.launch {
            try {
                previousShiftsListViewState.value = Loading
                val shifts = fetchPreviousShiftsUsecase.execute()
                previousShiftsListViewState.value = Success(shifts)
            } catch (throwable: Throwable) {
                previousShiftsListViewState.value = Error(mapToDomainException(throwable))
            }
        }
    }

    fun startShift(lastKnownLocation: Location) {
        viewModelScope.launch {
            try {
                startShiftViewState.value = Loading
                startShiftUsecase.execute(
                        StartOrEndShiftApiRequest(
                                getCurrentDateAndTimeStringInUTCFormat(),
                                lastKnownLocation.lat.toString(),
                                lastKnownLocation.long.toString()
                        )
                )
                startShiftViewState.value = Success(null)
            } catch (throwable: Throwable) {
                startShiftViewState.value = Error(mapToDomainException(throwable))
            }
        }
    }

    fun endShift(lastKnownLocation: Location) {
        viewModelScope.launch {
            try {
                endShiftViewState.value = Loading
                endShiftUsecase.execute(
                        StartOrEndShiftApiRequest(
                                getCurrentDateAndTimeStringInUTCFormat(),
                                lastKnownLocation.lat.toString(),
                                lastKnownLocation.long.toString()
                        )
                )
                endShiftViewState.value = Success(null)
            } catch (throwable: Throwable) {
                endShiftViewState.value = Error(mapToDomainException(throwable))
            }
        }
    }

    fun getPreviousShiftsListViewState(): LiveData<ViewState> = previousShiftsListViewState

    fun getPreviousShiftItemSelected(): LiveData<Shift> = previousShiftItemSelected

    fun setPreviousShiftItemSelected(item: Shift) {
        previousShiftItemSelected.value = item
    }

    fun getStartShiftViewState(): LiveData<ViewState> = startShiftViewState

    fun getEndShiftViewState(): LiveData<ViewState> = endShiftViewState
}