package com.deputy.shifts.presentation

import android.annotation.SuppressLint
import android.text.format.Time.getCurrentTimezone
import android.util.Log
import android.view.View
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}

@SuppressLint("SimpleDateFormat")
fun getCurrentDateAndTimeStringInUTCFormat(): String {
    val timeZone = TimeZone.getTimeZone("UTC")
    val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
    format.timeZone = timeZone
    return format.format(Date())
}

@SuppressLint("SimpleDateFormat")
fun convertUTCStringToDate(dateAndTime: String): Date {
    val timeZone = TimeZone.getTimeZone("UTC")
    val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
    format.timeZone = timeZone
    return format.parse(changeUTCTimeStringFormat(dateAndTime))!!
}

@SuppressLint("SimpleDateFormat")
fun getFormattedCurrentTimeString(): String {
    val timeFormat = SimpleDateFormat("hh:mm a")
    return timeFormat.format(Date()).toString()
}

fun getDateFromUTCString(utcDate: String): String {
    return utcDate.split("T")[0]
}

@SuppressLint("SimpleDateFormat")
fun getTimeFromUTCString(utcDate: String): String? {
    val format = SimpleDateFormat("hh:mm a")
    val timeZone = TimeZone.getTimeZone(getCurrentTimezone())
    format.timeZone = timeZone
    try {
        return format.format(convertUTCStringToDate(utcDate)).toString()
    } catch (exception: ParseException) {
        Log.e("getTimeFromUTCString", "Cannot parse date ${exception.message}")
    }
    return null
}

fun changeUTCTimeStringFormat(utcDate: String): String {
    val indexOfPlusChar = utcDate.indexOf("+")
    return if (indexOfPlusChar >= 0) {
        StringBuilder(utcDate.removeRange(indexOfPlusChar, utcDate.length)).append("Z").toString()
    } else {
        utcDate
    }
}
