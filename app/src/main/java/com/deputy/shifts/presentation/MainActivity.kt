package com.deputy.shifts.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.deputy.shifts.R

class MainActivity : AppCompatActivity(),
        ShiftDashboardFragment.OnPreviousShiftItemSelectedListener {

    private val showDashboardFragment = ShiftDashboardFragment.newInstance()

    companion object {
        private const val FRAGMENT_BACK_STACK = "fragment_back_stack"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            setupDashBoardFragment()
        }
    }

    private fun setupDashBoardFragment() {
        supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, showDashboardFragment)
                .commit()
    }

    override fun onPreviousShiftItemClicked() {
        showPreviousShiftLocationFragment()
    }

    private fun showPreviousShiftLocationFragment() {
        val showPreviousShiftLocationFragment = ShowPreviousShiftLocationFragment.newInstance()
        supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, showPreviousShiftLocationFragment)
                .addToBackStack(FRAGMENT_BACK_STACK)
                .commit()
    }
}