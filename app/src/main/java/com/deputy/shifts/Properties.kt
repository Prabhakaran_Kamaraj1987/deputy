package com.deputy.shifts

class Properties {
    companion object {
        const val API_BASE_URL = "https://apjoqdqpi3.execute-api.us-west-2.amazonaws.com/dmc/"
        const val API_PATH_GET_SHIFTS = "shifts"
        const val API_PATH_START_SHIFT = "shift/start"
        const val API_PATH_END_SHIFT = "shift/end"
        const val API_AUTH_HEADER = "prabha 40:7A:A1:A1:5C:FC:C6:66:C2:DA:B6:F7:58:5D:D7:45:23:BE:0F:7E"
        const val CONTENT_TYPE = "application/json"
        const val IS_LOGGING_ENABLED = true
        const val GOOGLE_MAPS_CAMERA_DEFAULT_ZOOM = 12.5f
    }
}