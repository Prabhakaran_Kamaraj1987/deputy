package com.deputy.shifts.injection.modules

import com.deputy.shifts.domain.usecases.EndShiftUsecase
import com.deputy.shifts.domain.usecases.FetchPreviousShiftsUsecase
import com.deputy.shifts.domain.usecases.StartShiftUsecase
import org.koin.dsl.module

val usecaseModule = module {
    factory { FetchPreviousShiftsUsecase(get()) }
    factory { StartShiftUsecase(get()) }
    factory { EndShiftUsecase(get()) }
}