package com.deputy.shifts.injection.modules

import com.deputy.shifts.Properties
import com.deputy.shifts.data.network.interceptors.ShiftApiInterceptor
import com.deputy.shifts.data.network.services.ShiftService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single { provideRetrofitInstance(get(), get()) }
    factory { provideOkHttpClient(get(), get()) }
    factory { provideHttpLoggingInterceptor() }
    factory { provideShiftApiInterceptor() }
    factory { provideShiftService(get()) }
    factory { provideGson() }
}

fun provideShiftService(retrofit: Retrofit): ShiftService {
    return retrofit.create(ShiftService::class.java)
}

fun provideRetrofitInstance(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
    return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(Properties.API_BASE_URL)
            .client(okHttpClient)
            .build()
}

fun provideOkHttpClient(
        httpLoggingInterceptor: HttpLoggingInterceptor,
        shiftApiInterceptor: ShiftApiInterceptor
): OkHttpClient {
    val builder = OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .addInterceptor(shiftApiInterceptor)
    return builder.build()
}

fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
    val loggingInterceptor = HttpLoggingInterceptor()
    loggingInterceptor.level = HttpLoggingInterceptor.Level.NONE
    if (Properties.IS_LOGGING_ENABLED) {
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    }
    return loggingInterceptor
}

fun provideShiftApiInterceptor() = ShiftApiInterceptor()

fun provideGson() = GsonBuilder().setLenient().create()