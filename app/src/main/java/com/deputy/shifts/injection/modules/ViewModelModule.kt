package com.deputy.shifts.injection.modules

import com.deputy.shifts.presentation.ShiftSharedViewModel
import org.koin.dsl.module

val viewModelModule = module {
    single { ShiftSharedViewModel(get(), get(), get()) }
}