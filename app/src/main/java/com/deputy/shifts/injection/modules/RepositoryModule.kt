package com.deputy.shifts.injection.modules

import com.deputy.shifts.domain.repositories.ShiftRepository
import com.deputy.shifts.domain.repositories.ShiftRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
    factory<ShiftRepository> { (ShiftRepositoryImpl(get())) }
}