package com.deputy.shifts.data.network.services

import com.deputy.shifts.Properties
import com.deputy.shifts.data.network.models.requests.StartOrEndShiftApiRequest
import com.deputy.shifts.data.network.models.response.ShiftResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ShiftService {
    @GET(Properties.API_PATH_GET_SHIFTS)
    suspend fun getPreviousShifts(): List<ShiftResponse>?

    @POST(Properties.API_PATH_START_SHIFT)
    suspend fun startShift(@Body request: StartOrEndShiftApiRequest)

    @POST(Properties.API_PATH_END_SHIFT)
    suspend fun endShift(@Body request: StartOrEndShiftApiRequest)
}
