package com.deputy.shifts.data.network.models.response

import com.google.gson.annotations.SerializedName

data class ShiftResponse(
    @SerializedName("id") val shiftId: Int,
    @SerializedName("start") val startTime: String,
    @SerializedName("end") val endTime: String,
    @SerializedName("startLatitude") val startLat: String,
    @SerializedName("startLongitude") val startLong: String,
    @SerializedName("endLatitude") val endLat: String,
    @SerializedName("endLongitude") val endLong: String,
    @SerializedName("image") val imageUrl: String
)
