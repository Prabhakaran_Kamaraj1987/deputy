package com.deputy.shifts.data.network.mappers

import com.deputy.shifts.data.network.models.response.ShiftResponse
import com.deputy.shifts.domain.models.Shift

class ShiftApiResponseMapper {
    companion object {
        fun mapToDomainObject(apiResponse: List<ShiftResponse>?) : List<Shift>? {
            if(apiResponse == null) return null
            val shifts = ArrayList<Shift>()
            apiResponse.forEach { shiftResponse ->
                val shift = Shift(
                    shiftResponse.shiftId,
                    shiftResponse.startTime,
                    shiftResponse.endTime,
                    shiftResponse.startLat,
                    shiftResponse.startLong,
                    shiftResponse.endLat,
                    shiftResponse.endLong,
                    shiftResponse.imageUrl)
                shifts.add(shift)
            }
            return shifts
        }
    }
}

