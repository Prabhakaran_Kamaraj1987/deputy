package com.deputy.shifts.data.network.models.requests

import com.google.gson.annotations.SerializedName

data class StartOrEndShiftApiRequest(
    @SerializedName("time") val time: String,
    @SerializedName("latitude") val lat: String,
    @SerializedName("longitude") val long: String,
)
