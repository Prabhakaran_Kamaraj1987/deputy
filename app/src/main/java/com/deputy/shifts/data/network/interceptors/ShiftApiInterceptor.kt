package com.deputy.shifts.data.network.interceptors

import com.deputy.shifts.Properties.Companion.API_AUTH_HEADER
import com.deputy.shifts.Properties.Companion.CONTENT_TYPE
import okhttp3.Interceptor
import okhttp3.Response

class ShiftApiInterceptor : Interceptor {
    // Add auth header and content type for every api call
    override fun intercept(chain: Interceptor.Chain): Response {
        val oldRequest = chain.request()
        val newRequest = oldRequest
                .newBuilder()
                .addHeader("Authorization", API_AUTH_HEADER)
                .addHeader("Content-Type", CONTENT_TYPE)
                .build()
        return chain.proceed(newRequest)
    }
}