package com.deputy.shifts.domain.usecases

import com.deputy.shifts.domain.models.Shift
import com.deputy.shifts.domain.repositories.ShiftRepository

class FetchPreviousShiftsUsecase (private val shiftRepository: ShiftRepository) {

    suspend fun execute(): List<Shift>? {
        return shiftRepository.getPreviousShifts()
    }
}