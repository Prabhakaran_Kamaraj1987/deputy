package com.deputy.shifts.domain.exceptions

import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

fun mapToDomainException(throwable: Throwable) : DomainException {
    return when (throwable) {
        is UnknownHostException,
        is ConnectException,
        is SocketTimeoutException -> NoNetworkException
        else -> GenericException
    }
}