package com.deputy.shifts.domain.repositories

import com.deputy.shifts.data.network.models.requests.StartOrEndShiftApiRequest
import com.deputy.shifts.domain.models.Shift

interface ShiftRepository {

    /**
     * Returns all previously completed shifts
     */
    suspend fun getPreviousShifts(): List<Shift>?

    /**
     * Starts the shift. Only one shift can be started at a time.
     */
    suspend fun startShift(request: StartOrEndShiftApiRequest)

    /**
     * Ends the shift. Good time to refresh the UI to show previous shifts.
     */
    suspend fun endShift(request: StartOrEndShiftApiRequest)
}