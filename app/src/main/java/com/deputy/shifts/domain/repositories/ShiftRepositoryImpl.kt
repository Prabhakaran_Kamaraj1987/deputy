package com.deputy.shifts.domain.repositories

import com.deputy.shifts.data.network.mappers.ShiftApiResponseMapper
import com.deputy.shifts.data.network.models.requests.StartOrEndShiftApiRequest
import com.deputy.shifts.data.network.services.ShiftService
import com.deputy.shifts.domain.models.Shift

class ShiftRepositoryImpl(private val shiftService: ShiftService) : ShiftRepository {

    override suspend fun startShift(request: StartOrEndShiftApiRequest) {
        shiftService.startShift(request)
    }

    override suspend fun endShift(request: StartOrEndShiftApiRequest) {
        shiftService.endShift(request)
    }

    override suspend fun getPreviousShifts(): List<Shift>? {
        return ShiftApiResponseMapper.mapToDomainObject(shiftService.getPreviousShifts())
    }
}