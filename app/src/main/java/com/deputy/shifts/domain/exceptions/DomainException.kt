package com.deputy.shifts.domain.exceptions

sealed class DomainException : Throwable()
object NoNetworkException : DomainException()
object GenericException : DomainException()