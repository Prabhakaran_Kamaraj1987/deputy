package com.deputy.shifts.domain.models

data class Location(val lat: Double, val long: Double)
