package com.deputy.shifts.domain.models

/**
 * Represents a previously completed Shift. Contains information about when the shift was started
 * and ended with their respective location information.
 */
data class Shift(
    val shiftId: Int,
    val startTime: String,
    val endTime: String,
    val startLat: String,
    val startLong: String,
    val endLat: String,
    val endLong: String,
    val imageUrl: String,
)
