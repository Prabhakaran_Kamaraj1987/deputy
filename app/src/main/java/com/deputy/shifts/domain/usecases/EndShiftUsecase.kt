package com.deputy.shifts.domain.usecases

import com.deputy.shifts.data.network.models.requests.StartOrEndShiftApiRequest
import com.deputy.shifts.domain.repositories.ShiftRepository

class EndShiftUsecase (private val shiftRepository: ShiftRepository) {

    suspend fun execute(request: StartOrEndShiftApiRequest) {
        shiftRepository.endShift(request)
    }
}