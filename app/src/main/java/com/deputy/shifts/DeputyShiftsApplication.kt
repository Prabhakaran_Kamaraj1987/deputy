package com.deputy.shifts

import android.app.Application
import com.deputy.shifts.injection.modules.networkModule
import com.deputy.shifts.injection.modules.repositoryModule
import com.deputy.shifts.injection.modules.usecaseModule
import com.deputy.shifts.injection.modules.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class DeputyShiftsApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@DeputyShiftsApplication)
            modules(
                    listOf(
                            networkModule,
                            repositoryModule,
                            usecaseModule,
                            viewModelModule
                    )
            )
        }
    }
}